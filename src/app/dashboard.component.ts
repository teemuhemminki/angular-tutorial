import { Component, OnInit } from '@angular/core';

import { Hero } from './hero';
import { HeroService } from './hero.service';

@Component({
  selector: 'app-my-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

  heroes: Hero[] = [];

  /* Alla toteutetaan dependency injectionia. Sen sijaan että DashboardComponent
  Loisi itselleen uuden heroServicen, se käyttää jo olemassaolevaa serviceä injektoimalla
  sen itseensä. Näin kaikki muutokset voidaan toteuttaa heroServicessä ilman että
  tarvitsee muuttaa parametrejä ja samasta palvelusta voidaan toteuttaa vain yksi instanssi.
  Käytännössä DI on referenssin antamista.
  */
  constructor(private heroService: HeroService) { }

  /* Käytetään lifecycle metodia eli hookkia. ngOnInit ajetaan kun DashboardComponent
  instanssioidaan, mutta vasta constructorin jälkeen. */
  ngOnInit(): void {
    this.heroService.getHeroes()
      .then(heroes => this.heroes = heroes.slice(1, 5));
  }
}
