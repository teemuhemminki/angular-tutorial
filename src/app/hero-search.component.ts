import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/observable/of';

// rxjs operaattoreita jotka auttavat ehkäisemään turhaa datavirtaa ja käsittelemään virheitä
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { HeroSearchService } from './hero-search.service';
import { Hero } from './hero';

@Component({
  selector: 'app-hero-search', // Kertoo millä sanalla tätä komponenttia voi hakea html koodissa
  templateUrl: './hero-search.component.html',
  styleUrls: [ './hero-search.component.css' ],
  providers: [HeroSearchService]
})

export class HeroSearchComponent implements OnInit {
  heroes: Observable<Hero[]>;
  private searchTerms = new Subject<string>(); // Tuottaa Subject Observablen (kts. importit)

  constructor(
    private heroSearchService: HeroSearchService,
    private router: Router) {}

  search(term: string): void {
    this.searchTerms.next(term); // Asetetaan haettava sana searchTerms Observablen ratkottavaksi.
  }

  ngOnInit(): void {
    this.heroes = this.searchTerms // Koska subject on observable, voidaan subject tyypin searcTerms asettaa Observable heroihin
      // Seuraavat kaksi komentoa estävät turhaa dataliikennettä
      .debounceTime(300)        // Odotetaan 300ms jokaisen näppäin painalluksen jälkeen, ennen kuin aloitetaan haku
      .distinctUntilChanged()   // Toteutetaan haku vain jos hakusana on muuttunut
      .switchMap(term => term   // Kutsuu hakupalvelun
        ? this.heroSearchService.search(term) // Palautetaan http search Observable
        : Observable.of<Hero[]>([])) // Tai jos ei ole hakusanaa, palautetaan tyhjä taulukko
      .catch(error => {
        console.log(error);
        return Observable.of<Hero[]>([]);
      });
  }

  gotoDetail(hero: Hero): void {
    const link = ['/detail', hero.id]; // Tutoriaalissa käytettiin let komentoa, mutta jslint neuvoi vaihtamaan constiin. Molemmat toimivat.
    this.router.navigate(link);
  }
}
