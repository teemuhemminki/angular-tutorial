/*
Router eli reititin luo linkit eri komponentteihin joihin käyttäjällä on pääsy.
Reittien kautta avataan komponentti, joka tuo viewinsä ja toimintonsa viewiin.
*/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { HeroesComponent } from './heroes.component';
import { HeroDetailComponent } from './hero-detail.component';

const routes: Routes = [ // Tuodaan reitit ohjelmaan
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard',  component: DashboardComponent },
  { path: 'detail/:id', component: HeroDetailComponent }, // :id hakee vastaavan arvon mukaista sankaria
  { path: 'heroes',     component: HeroesComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
