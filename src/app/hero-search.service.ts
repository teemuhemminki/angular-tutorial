import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Hero } from './hero';

@Injectable()
export class HeroSearchService {

  constructor(private http: Http) {} // Initialisoidaan http olio

  // Search hakee term mukaisen sankarin ja palauttaa sen Observable olion taulukossa
  search(term: string): Observable<Hero[]> {
    return this.http
            .get(`api/heroes/?name=${term}`)
            // Haettu Hero data muokataan map operaattorin avulla jsonista taulukoksi
            // Map operaattori muistuttaa perus javascriptin array.prototype.map funktiota
            .map(response => response.json().data as Hero[]);
  }
}
