/* Tuotiin hero luokka omaan tiedostoonsa.
Angularissa on hyvien käytänteiden mukaista pitää yksi luokka per tiedosto.
*/

export class Hero {
  id: number;
  name: string;
}
