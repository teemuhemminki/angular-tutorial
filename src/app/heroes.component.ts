import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Hero } from './hero';
import { HeroService } from './hero.service';

@Component({
  selector: 'app-my-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css'],
  providers: [HeroService] // Providers taulukko luo HeroService instanssin kun AppComponent luodaan.
})

// AppComponenttia extendataan OnInit luokalla, joka siis ajaa toiminnon kun komponentti käynnistyy
export class HeroesComponent implements OnInit {
  constructor (
    private heroService: HeroService,
    private router: Router
  ) { } // Luodaan HeroService referenssi, näin palvelu injektoidaan AppComponentille

  heroes: Hero[]; // Tehdään muuttuja sankari taulukolle
  selectedHero: Hero;

  onSelect(hero: Hero): void { // Tehdään funktio jolla valitaan sankari
    this.selectedHero = hero;
  }

  getHeroes(): void {
    /* Kun heroService palauttaa promisensa, toteutetaan anonyymi funktio jossa
    this.heroes asetetaan heroServicen callbackin palauttamaksi heroeksi */
    this.heroService.getHeroes().then(heroes => this.heroes = heroes);
  }

  ngOnInit(): void { // Ajetaan kun olio initialisoidaan. Käytännössä hakee heroServiceltä hero taulukon.
    this.getHeroes();
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedHero.id]);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.create(name)
      .then(hero => {
        this.heroes.push(hero);
        this.selectedHero = null;
      });
  }

  delete(hero: Hero): void {
      this.heroService
        .delete(hero.id)
        .then(() => {
          this.heroes = this.heroes.filter(h => h !== hero);
          if (this.selectedHero === hero) { this.selectedHero = null; }
        });
  }
}
