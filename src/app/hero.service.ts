/* HeroService on service, eli tarjoaa komponenteille palveluja.
Tässä tapauksessa hakee datan in-memory-data.service.ts tiedostosta.
Tosielämässä hakisi datan palvelimen tietokannasta. */

import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise'; // Tuodaan rxjs kirjastosta Observablen muokkaukseen liittyvä operaattori

import { Hero } from './hero';

/* Kerrotaan että HeroService luokka on injectable ja injector voi luoda siitä instanssin
Tätä ei tarvitse toteuttaa komponenteille, koska @component on @injectablen alaluokka */
@Injectable()

export class HeroService {

  private heroesUrl = 'api/heroes'; // URL web apiin

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }

  getHeroes(): Promise<Hero[]> { // getHeroes luo promisen, että antaa datan kun saa sen haettua.
    return this.http.get(this.heroesUrl) // http.get Palauttaa RxJS Observablen joka on keino hallita asynkronista datavirtaa
                .toPromise() // Muokkaa saadun observablen promiseksi
                .then(response => response.json().data as Hero[]) // Tuodaan JSON data Hero taulukoksi
                // Virheen sattuessa se käsitellään, http:n kanssa voi aina tulla virheitä ja niihin täytyy aina varautua catchilla
                .catch(this.handleError);
  }

  // GetHeroesSlowlylla voidaan simuloida latenssia
  getHeroesSlowly(): Promise<Hero[]> {
    return new Promise(resolve => {
      setTimeout(() => resolve(this.getHeroes()), 2000); // Annetaan 2 sekunnin viive
    });
  }

  getHero(id: number): Promise<Hero> {
      const url = `${this.heroesUrl}/${id}`;
      return this.http.get(url)
        .toPromise()
        .then(response => response.json().data as Hero)
        .catch(this.handleError);
  }

  update(hero: Hero): Promise<Hero> {
      const url = `${this.heroesUrl}/${hero.id}`;
      return this.http
        .put(url, JSON.stringify(hero), {headers: this.headers})
        .toPromise()
        .then(() => hero)
        .catch(this.handleError);
  }

  create(name: string): Promise<Hero> {
    return this.http
      .post(this.heroesUrl, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then( res => res.json().data as Hero )
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
      const url = `${this.heroesUrl}/${id}`;
      return this.http.delete(url, {headers: this.headers})
        .toPromise()
        .then(() => null)
        .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error); // Tosikäytössä virheilmoitus pitäisi tuoda sivulle käyttäjän näkyviin
    return Promise.reject(error.message || error);
  }
}
