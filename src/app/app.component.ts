/* Juuri komponentti. Komponentit ovat angularissa rakennuspalikoita,
joilla voi muodostaa hierarkkisen rakenteen.
Komponentti kontrolloi viewiä ja siinä tapahtuvia toimintoja.
Teknisesti ottaen komponentti on myös direktiivi templaatilla. */

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html', // Templaatti haetaan urlin kautta ja siksi muutetaan urlin mukaista app.component.html tiedostoa
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Tour of Heroes';
}
