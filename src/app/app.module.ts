import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // Formien käsittelyyn liittyvät toiminnot, kuten ngModel haetaan täältä
import { HttpModule } from '@angular/http'; // Haetaan http moduuli http palveluiden käyttämistä varten.

 // Etäserveriä simuloivat paketit piti cli projektissa asentaa 'npm install angular-in-memory-web-api --save' komennolla.
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { AppComponent } from './app.component';
import { HeroDetailComponent } from './hero-detail.component';
import { HeroesComponent } from './heroes.component';
import { HeroService } from './hero.service';
import { DashboardComponent } from './dashboard.component';
import { HeroSearchComponent } from './hero-search.component';

import {AppRoutingModule } from './app-routing.module';

@NgModule({
// Määritellään mitkä komponentit, direktiivit ja filtterit kuuluvat moduuliin
  declarations: [
    AppComponent,
    DashboardComponent,
    HeroDetailComponent,
    HeroesComponent,
    HeroSearchComponent
  ],
  // Tuodaan tukevia ulkoisia moduuleja käytettäväksi
  imports: [
    BrowserModule,
    FormsModule, // Importtaa FormsModulen jotta templaatti voi bindata [(ngModel)]
    AppRoutingModule,
    HttpModule, // Tuo http moduulin käytettäväksi
    InMemoryWebApiModule.forRoot(InMemoryDataService) // Simuloi etäserveri kommunikaatiota
  ],
  // Tuo palvelut Dependency Injectionin tietoon
  providers: [
    HeroService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
