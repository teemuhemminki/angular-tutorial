# Angular tutorial
This is my personal execution of official angular tutorial. It is commented in Finnish and made as an assignment for JAMK course of Web Application Development.

Assignment related question:  
Q: What benefit is there for recovering data from service and why is it used in tutorial before implementing http?  
A: With promise the app doesn't need to stop and wait for the answer from server, but it instead gains answer asynchronically when results are ready.  
This data recovery is handled by service, so that same code doesn't have to be rewritten but can instead be called from single place. Any necessary edits need only to happen within the service itself.  
If project is going to use http protocol, it is useful to develop with promises even if http isn't yet implemented. This way the latency handling can be tested during the production.  


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.2.
Remember to run `npm install` after cloning this repository.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
